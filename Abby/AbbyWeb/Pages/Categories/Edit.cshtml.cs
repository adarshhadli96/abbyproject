using AbbyWeb.Data;
using AbbyWeb.Models;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.ModelBinding;
using Microsoft.AspNetCore.Mvc.RazorPages;

namespace AbbyWeb.Pages.Categories
{
    [BindProperties]
    public class EditModel : PageModel
    {
        private readonly ApplicationDbContext _db;
        

        //Added bind property here for much simpler use
        //[BindProperty]
        public Category Category { get; set; }

        public EditModel(ApplicationDbContext db)
        {
            _db= db;
        }


        public void OnGet(int id)
        {
            Category = _db.Category.Find(id);
            //Category = _db.Category.First(u => u.Id == id);
            //Category = _db.Category.FirstOrDefault(u => u.Id == id);
            //Category = _db.Category.Single(u => u.Id == id);
            //Category = _db.Category.SingleOrDefault(u => u.Id == id);

        }

        //public async Task<IActionResult> OnPost(Category category)
        //{
        //    await _db.Category.AddAsync(category);
        //    await _db.SaveChangesAsync();
        //    return RedirectToPage("Index");
        //}

        //Above code can be modified for much simpler using bindproperty annotation

        public async Task<IActionResult> OnPost()
        {
            // custom validations
            if (Category.Name == Category.DisplayOrder.ToString())
            {
                ModelState.AddModelError("Category.Name", "Display order cannot exactly match the name");
            }


            // server side validation
            if (ModelState.IsValid)
            {
                 _db.Category.Update(Category);
                await _db.SaveChangesAsync();
                TempData["success"] = "Category updated successfully";
                return RedirectToPage("Index");
            }
            return Page();
        }
    }
}
