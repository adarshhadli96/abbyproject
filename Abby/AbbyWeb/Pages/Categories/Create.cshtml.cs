using AbbyWeb.Data;
using AbbyWeb.Models;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.ModelBinding;
using Microsoft.AspNetCore.Mvc.RazorPages;

namespace AbbyWeb.Pages.Categories
{
    [BindProperties]
    public class CreateModel : PageModel
    {
        private readonly ApplicationDbContext _db;
        

        //Added bind property here for much simpler use
        //[BindProperty]
        public Category Category { get; set; }

        public CreateModel(ApplicationDbContext db)
        {
            _db= db;
        }


        public void OnGet()
        {

        }

        //public async Task<IActionResult> OnPost(Category category)
        //{
        //    await _db.Category.AddAsync(category);
        //    await _db.SaveChangesAsync();
        //    return RedirectToPage("Index");
        //}

        //Above code can be modified for much simpler using bindproperty annotation

        public async Task<IActionResult> OnPost()
        {
            // custom validations
            if (Category.Name == Category.DisplayOrder.ToString())
            {
                ModelState.AddModelError("Category.Name", "Display order cannot exactly match the name");
            }


            // server side validation
            if (ModelState.IsValid)
            {
                await _db.Category.AddAsync(Category);
                await _db.SaveChangesAsync();
                TempData["success"] = "Category created successfully";
                return RedirectToPage("Index");
            }
            return Page();
        }
    }
}
